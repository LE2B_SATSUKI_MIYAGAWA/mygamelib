#pragma once
#include "GameBase.h"
#include "Scene/BaseScene.h"
#include "Scene/TitleScene.h"
#include "Scene/GameScene.h"
#include "Scene/EndScene.h"
#include "3d/ParticleManager.h"

class DirectXGame : public GameBase
{
public:
    void Initialize() override;

	virtual void Update() override;

	virtual void Draw() override;

	virtual void Finalize() override;

private:
	BaseScene* scene = nullptr;
	Scene sceneState;
};

