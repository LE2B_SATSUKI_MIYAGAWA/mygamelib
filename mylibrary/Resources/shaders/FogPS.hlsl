#include "Fog.hlsli"

//レイリー散乱
float RayleighPhaseFunction(float fCos2)
{
	return 3.0f / (16.0f * 3.14159265) * (1.0f + fCos2);
}
//ミー散乱
float MiePhaseFunction(float g, float fCos)
{
	return (1 - g) * (1 - g) / (4.0f * 3.14159265 * pow(1 + g * g - 2.0f * g * fCos, 1.5f));
}

//ランダムベクトル取得
float2 randomVec(float2 fact)
{
	float2 angle = float2(
		dot(fact, float2(127.1f, 311.7f)),
		dot(fact, float2(269.5f, 183.3f))
		);

	return frac(sin(angle) * 43758.5453123) * 2 - 1;
}

//ノイズの密度をdensityで設定,uvにi.uvを代入  
float PerlinNoise(float density, float2 uv)
{
	float2 _ScreenParams = float2(1280, 720);
	float2 uvFloor = floor(uv * density * _ScreenParams.xy);
	float2 uvFrac = frac(uv * density * _ScreenParams.xy);

	float2 v00 = randomVec(uvFloor + float2(0, 0));//各頂点のランダムなベクトルを取得
	float2 v01 = randomVec(uvFloor + float2(0, 1));
	float2 v10 = randomVec(uvFloor + float2(1, 0));
	float2 v11 = randomVec(uvFloor + float2(1, 1));

	float c00 = dot(v00, uvFrac - float2(0, 0));//内積を取る
	float c01 = dot(v01, uvFrac - float2(0, 1));
	float c10 = dot(v10, uvFrac - float2(1, 0));
	float c11 = dot(v11, uvFrac - float2(1, 1));

	float2 u = uvFrac * uvFrac * (3 - 2 * uvFrac);

	float v0010 = lerp(c00, c10, u.x);
	float v0111 = lerp(c01, c11, u.x);

	return lerp(v0010, v0111, u.y) / 2 + 0.5f;
}

//フラクタル和
float FractalSumNoise(float density, float2 uv)
{
	float fn;
	fn = PerlinNoise(density * 1, uv) * 1.0 / 2;
	fn += PerlinNoise(density * 2, uv) * 1.0 / 4;
	fn += PerlinNoise(density * 4, uv) * 1.0 / 8;
	fn += PerlinNoise(density * 8, uv) * 1.0 / 16;

	return fn;
}

//float4 main(PSInput input) : SV_TARGET
//float4 main(VSOutput input) : SV_TARGET

float4 main(PSInput input) : SV_TARGET
{
	 float4 texcolor = tex.Sample(smp,input.uv);
	 ////定数フォグ
	 float depth = tex0.Sample(smp, input.uv);
	 //正規デバイス座標系での座標
	 float4 viewPos = float4(0, 0, depth, 1);
	 //プロジェクション逆行列
	 viewPos = mul(inverse, viewPos);
	 //W除算
	 viewPos.z /= viewPos.w;

	 float fogWeight = 0.0f;
	 float fog_Scale = 1.0f;
	 //霧減数率
	 float g = 0.04f;

	 fogWeight += fog_Scale * max(0.0f, 1.0f - exp(-g * viewPos.z));

	 float4 bgColor = tex.Sample(smp, input.uv);
	 const float sunCos = 8.84f;
	 //const float sunCos = 6.84f;
	 const float mieCoeffG = -0.75f;
	 const float betaPhaseR = RayleighPhaseFunction(sunCos * sunCos);
	 const float betaPhaseM = MiePhaseFunction(mieCoeffG, sunCos);
	 const float rayleighCoeff = 1.0f;
	 const float4 rayleighColor = float4(0.9f, 0.9f, 0.9f, 1.0f);
	 //const float mieCoeff = 1.8f;
	 const float mieCoeff = 3.84f;
	 const float4 mieColor = float4(0.8f, 0.6f, 0.0f, 1.0f);

	 ////砂嵐
	 float density = 0.001f;
	 float t = Time.x;
	 float t2 = Time.y;
	 float pn = FractalSumNoise(0.02f,    input.uv + +float2(t, t));
	 float pn2 = FractalSumNoise(density, input.uv + float2(t2, t2));
	 float pn3 = FractalSumNoise(0.015f, input.uv + float2(-t2, -t2));

	 float  noise = pn3 * pn2 * pn * fogWeight;
	 float4 fogColor = (betaPhaseR * rayleighCoeff * rayleighColor + betaPhaseM * mieCoeff * mieColor) / (rayleighCoeff + mieCoeff) + noise;
	 float4 outputColor = lerp(bgColor, fogColor, fogWeight);

	 //ノーマルマップ
	 //float3 norm = normalize((tex1.SampleLevel(smp, input.uv, 0).xyz - 0.5));

	 //float3 lightDir = normalize(float3(-1, -1, 0));
	 //float l = saturate(dot(norm, lightDir));

	 //float4 diffuse = float4(0.9f, 0.9f, 0.9f,1.0f) * l;

	 return outputColor;
	 //return outputColor * diffuse;
	 //return diffuse;
}