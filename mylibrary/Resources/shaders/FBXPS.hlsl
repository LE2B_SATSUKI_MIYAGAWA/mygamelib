#include "FBX.hlsli"
//0番スロットに設定されたテクスチャ
Texture2D<float4> tex : register(t0);
//0番スロットに設定されたサンプラー
SamplerState smp : register(s0);
//エントリーポイント
float4 main(VSOutput input) : SV_TARGET
{
	//テクスチャマッピング
	//float4 texcolor = tex.Sample(smp,input.uv);
	////Lambert反射
	//float3 light = normalize(float3(1, -1, -1));
	//float  diffuse = saturate(dot(-light, input.normal));
	//float  brightness = diffuse + 0.3f;
	//float4 shadecolor = float4(brightness, brightness, brightness, 1.0f);
	//////陰影とテクスチャの色を合成
	//return shadecolor * texcolor;

	////シェーディングによる色
	//float4 shadeColor;
	////光沢度
	//const float shininess = 20.0f;
	////頂点から視点への方向ベクトル
	//float3 eyeDir = normalize(cameraPos - input.svpos.xyz);
	////ライトに向かうベクトルと法線の内積
	//float3 dotLightNormal = dot(-light, input.normal);
	////反射光ベクトル
	//float3 reflect = normalize(light + 2 * dotLightNormal * input.normal);
	////環境反射光
	//float3 ambient = float3(1,1,1);
	//float4 m_diffuse = float4(1, 1, 1, 1);
	////拡散反射光
	////float3 diffuse = dotLightNormal * (m_diffuse.rgb * m_diffuse.w);
	////鏡面反射光
	//float3 specular = pow(saturate(dot(reflect, eyeDir)), shininess) * (reflect.rgb * reflect);
	//float3 lightColor = float3(1, 1, 1);

	//shadecolor.rgb = (ambient + diffuse + specular) * lightColor;
	//shadecolor.a = 1.0f;

	//float4 color = float4(1,1,1,1);

	//float4 phong = texcolor * shadecolor * color;

	//return phong;

	//トゥーンシェーダー
	float3 light = normalize(float3(1, 1, 1)); // 右下奥　向きのライト
	float diffuse = saturate(dot(-light, input.normal));
	float specular = pow(diffuse, 100);

	if (diffuse > 0.75f)
	{
		diffuse = 1.0f;
	}
	else  if (diffuse > 0.5f)
	{
		diffuse = 0.5f;
	}
	else if(diffuse > 0.35f)
	{
		diffuse = 0.25f;
	}
	else
	{
		diffuse = 0.20f;
	}

	//float4 brightness = float4(diffuse + 0.4f, diffuse + 0.6f, diffuse + 1.0f,1);
	float4 brightness = diffuse + 0.3f;
	float4 texcolor = tex.Sample(smp, input.uv);
	return float4(texcolor.rgb * brightness + float3(specular, specular, specular), texcolor.a);
}