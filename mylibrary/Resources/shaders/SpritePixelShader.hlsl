#include "SpriteShaderHeader.hlsli"

Texture2D<float4> tex : register(t0);//0番スロットに設定されたテクスチャ
SamplerState smp : register(s0); //0番スロットに設定されたサンプラー

float4 PSmain(VSOutput input) : SV_TARGET
{
	//return tex.Sample(smp,input.uv) * color;

	////樽状湾曲
	//float2 samplePoint = input.uv;
	//samplePoint -= float2(0.5, 0.5);
	//float distPower = pow(length(samplePoint), 0.5);
	//samplePoint *= float2(distPower, distPower);
	//samplePoint += float2(0.5, 0.5);
	//float4 Tex = tex.Sample(smp, samplePoint);
	
	////ビネット
	float2 samplePoint = input.uv;
	float4 Tex2 = tex.Sample(smp, samplePoint);
	float vignette = length(float2(0.5, 0.5) - input.uv);
	vignette = clamp(vignette - 0.3, 0, 0.8);
	Tex2.rgb -= vignette;

	//return Tex + Tex2 * Time;
	//return Tex * Time;
	return Tex2 * Time;
}