#include "Fog.hlsli"

//HSInput main(float4 pos : POSITION, float2 uv : TEXCOORD)
//VSOutput main(float4 pos : POSITION, float2 uv : TEXCOORD)

HSInput main(float4 pos : POSITION, float2 uv : TEXCOORD)
{
	//HSInput output; // ハルシェーダーに渡す値
	VSOutput output; // ハルシェーダーに渡す値

	output.svpos = pos;
	output.uv = uv;
	pos = mul(viewproj, pos);
	float4x4 mtxWVP = mul(world, viewproj);

	return output;
}