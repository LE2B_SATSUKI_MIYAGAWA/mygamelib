cbuffer cbuff0 : register(b0)
{
	matrix inverse;//ビュー逆行列
	float4x4 viewproj;//ビュープロジェクション行列
	float4x4 world;
	float4 tessRange;
	float4 Animation;
	float4 Time;
	float3 cameraPos;
};

Texture2D<float> tex : register(t0);  // 0番スロットに設定されたテクスチャ
Texture2D<float4> tex0 : register(t1);//深度テクスチャ
Texture2D<float4> tex1 : register(t2);//法線マップテクスチャ

SamplerState smp : register(s0);      // 0番スロットに設定されたサンプラー

//頂点シェーダーからピクセルシェーダーへのやり取りに使用する構造体
struct VSOutput
{
	float4 svpos :SV_POSITION;//システム用頂点座標
	float2 uv :TEXCOORD;   //uv値
};

struct HSInput
{
	float4 svpos : SV_POSITION;
	float2 uv : TEXCOORD0;
};

struct DSInput
{
	float4 svpos : SV_POSITION;
	float2 uv : TEXCOORD0;
};

struct PSInput
{
	float4 svpos : SV_POSITION;
	float2 uv : TEXCOORD0;
	float3 Normal : TEXCOORD1;
};

struct HSParameters
{
	float tessFactor[4] : SV_TessFactor;
	float insideFactor[2] : SV_InsideTessFactor;
};
