#include "TestFog.hlsli"

VSOutput main(float4 pos : POSITION,float2 uv : TEXCOORD)
{
	VSOutput output; // ピクセルシェーダーに渡す値
	output.svpos = pos;
	output.uv.xy = uv;
	pos = mul(viewproj, pos);

	return output;
}

//VSOutput main(float4 pos : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD)
//{
//	float4 wnormal = normalize(mul(world, float4(normal, 0)));
//
//	VSOutput output; // ピクセルシェーダーに渡す値
//	output.svpos = pos;
//	output.uv.xy = uv;
//	//output.normal = normal;
//	output.normal = wnormal.xyz;
//	//pos = mul(viewproj, pos);
//
//	return output;
//}