#include "EndScene.h"
#include "TitleScene.h"

EndScene::EndScene()
{
	scenePattern = Scene::End;
}

EndScene::~EndScene()
{
}

void EndScene::Initialize(Direcx12Base* dxCommon, Input* input, AudioManager* audio, TextureManager* tMng, DebugCamera* dCamera, BackCamera* bCamera, Gamepad* gamepad, DebugText* dText)
{
	BaseScene::Initialize(dxCommon, input, audio, tMng, dCamera, bCamera, gamepad, dText);
	SceneChengeFlag = false;

	audio->Stop();

	//サウンドの読み込み
	audio->PlayLoop(L"Resources/sounds/End.wav");
	//テクスチャ読み込み
	tMng->spriteLoadTexture(7, L"Resources/textures/End.png");

	end = std::make_unique<Sprite>();
	end->Initialize(7);
	end->SetSize(1280, 720);
	time = 0;
	fadeOutFlag = false;
}

void EndScene::Update()
{
	// Enterで指定のシーンへ
	if (input->TriggerPush(DIK_SPACE) && !fadeOutFlag)
	{
		audio->PlayWave(L"Resources/sounds/button06 .wav");
		fadeOutFlag = !fadeOutFlag;
	}

	if (fadeOutFlag)
	{
		time++;
	}

	if (time >= 10)
	{
		end->ChengeFadeOutFlag();
		ChangeSceneChengeFlag();
	}

	if (time >= 370)
	{
		//ゲームシーンへ
		nextScene = new TitleScene();
	}
}

void EndScene::Draw()
{
}

void EndScene::UIDraw()
{
	// コマンドリストの取得
	ID3D12GraphicsCommandList* cmdList = dxCommon->GetCommandList();
	//スプライトのパイプラインをセット
	Sprite::SetPipelineState(dxCommon->GetCommandList());
	// 前景スプライト描画前処理
	Sprite::PreDraw(cmdList);
	end->Draw(dxCommon->GetCommandList());
	// スプライト描画後処理
	Sprite::PostDraw();
}

void EndScene::BgDraw()
{
}

void EndScene::FogDraw()
{
}
