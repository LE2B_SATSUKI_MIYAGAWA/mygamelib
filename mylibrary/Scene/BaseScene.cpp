#include "BaseScene.h"
#include <cassert>

BaseScene::BaseScene()
{
}

BaseScene::~BaseScene()
{
}

void BaseScene::Initialize(Direcx12Base* dxCommon, Input* input, AudioManager* audio, TextureManager* tMng,
	DebugCamera* dCamera, BackCamera* bCamera, Gamepad* gamepad, DebugText* dText)
{
	// nullptr�`�F�b�N
	assert(dxCommon);
	assert(input);
	assert(audio);
	assert(tMng);
	assert(dCamera);
	assert(bCamera);
	assert(gamepad);
	assert(dText);

	this->dxCommon = dxCommon;
	this->input = input;
	this->audio = audio;
	this->tMng = tMng;
	this->dCamera = dCamera;
	this->bCamera = bCamera;
	this->gamepad = gamepad;
	this->dText = dText;
}