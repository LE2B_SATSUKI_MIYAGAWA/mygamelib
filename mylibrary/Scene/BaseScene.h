#pragma once
#include "GameBase.h"

enum class Scene
{
    Title,
	Game,
	End
};

class BaseScene
{
public:
	BaseScene();
	virtual ~BaseScene();

	virtual void Initialize(Direcx12Base* dxCommon, Input* input, AudioManager* audio, TextureManager* tMng,
		DebugCamera* dCamera, BackCamera* bCamera, Gamepad* gamepad, DebugText* dText);

	virtual void Update() = 0;

	virtual void Draw() = 0;

	virtual void UIDraw() = 0;

	virtual void BgDraw() = 0;

	virtual void FogDraw() = 0;

	BaseScene* GetNextScene() { return nextScene; }
	Scene GetScene() { return scenePattern; }

	bool& GetSceneChengeFlag() { return SceneChengeFlag; }

	bool& ChangeSceneChengeFlag() { return SceneChengeFlag = !SceneChengeFlag; }

protected:
	Direcx12Base* dxCommon = nullptr;
	Input* input = nullptr;
	AudioManager* audio = nullptr;
	BaseScene* nextScene = nullptr;
	TextureManager* tMng = nullptr;
	DebugCamera* dCamera = nullptr;
	BackCamera* bCamera = nullptr;
	Gamepad* gamepad = nullptr;
	DebugText* dText = nullptr;
	Scene scenePattern;
	bool SceneChengeFlag;
};

