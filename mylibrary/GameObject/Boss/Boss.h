#pragma once
#include "3d/Object3d.h"
#include "Math/Easing.h"
#include "Input/Input.h"
#include "Player.h"
#include "Collision/SphereCollider.h"
#include "3d/AttackEnemyCollisionObject.h"
#include "Math/Matrix4.h"
#include "BossBaseState.h"
#include "Audio/AudioManager.h"
#include <memory>

class BossBaseState;
class AttackEnemyCollisionObject;
class Player;
class Boss :
    public Object3d, public std::enable_shared_from_this<Boss>
{
public://エイリアス
	using XMMATRIX = DirectX::XMMATRIX;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMVECTOR = DirectX::XMVECTOR;
	using shared_ptrState = std::shared_ptr<BossBaseState>;

public:
	/// <summary>
	/// 3Dオブジェクト生成
	/// </summary>
	/// <returns></returns>
	//static Boss* Create(Model* model = nullptr);
	static std::shared_ptr<Boss> Create(Model* model = nullptr);
public:
	/// <summary>
	/// 初期化
	/// </summary>
	/// <returns>成否</returns>
	bool Initialize();

	/// <summary>
	/// 毎フレーム処理
	/// </summary>
	void Update(Player* player, AttackEnemyCollisionObject* AttackCollision, AudioManager* audio);

	/// <summary>
    /// 補正座標をセットする
    /// </summary>
    /// <param name="distance">距離</param>
    /// <param name="correctionPosY">補正するY座標</param>
	Vector3 GetCorrectionPos(const float& distance, Vector3& correctionPosY)
	{
		return ((atan2f(Ppos.x - position.x, Ppos.z - position.z) * 60) * distance) + correctionPosY;
	}

	//SetterGetter
	float& GetTimeRate() { return timeRate; }
	float& GetTime();
	float& GetAngle() { return angle; }
	Vector3& Getresult() { return Vpos; }

	float& GetDotVector() { return dotPos; }
	float& GetradiusCos() { return radiuncos; }
	float GetAngle(int mx, int my, int sx, int sy) {
		return short(atan2f(-float(sy - my), float(sx - mx)));
	}

	SphereCollider& GetSphereCollider() { return *sphereCollider;}

	bool& GetRoarFlag();
	bool& GetColFlag();
	void ChangeState(std::shared_ptr<BossBaseState> weak_bossState);
	bool ChangeLifeFlag() { return lifeFlag = !lifeFlag; }
	bool GetLifeFlag() { return lifeFlag;}

	shared_ptrState GetState() { return shared_bossState; }
private:
	std::shared_ptr<BossBaseState> shared_bossState;
	Input* input = Input::GetInstance();// マウスの入力を取得
	Vector3 Vpos;     //移動量のベクトル座標
	Vector3 Ppos;     //プレイヤーの座標
	Vector3 Bpos;
	Vector3 EaseStart;//イージング開始座標

	Vector3 REaseStart;//イージング開始座標
	Vector3 EaseEnd;  //イージング終了座標
	Vector3 AttackPos;
	SphereCollider* sphereCollider;
	Vector3 forwardVector;//前方向ベクトル
	Matrix4 warldMat;//ワールド座標行列
	Vector3 walrdPos;//ワールド座標ベクトル
	Vector3 walrdPos2;//ワールド座標ベクトル
	Vector3 sa;

	float dotPos;
	float radiuncos;
	//時間系
	float maxTime;//全体時間[s]
	float timeRate;//何％時間が進んだか(率)
	float elapsedTime;
	float totalTime;//進めた時間
	bool timerFlag;    //タイマーが起動しているかどうか
	float angle;
	float bai;
	bool lifeFlag;
	float radius;
	Vector3 angleVector;
};

