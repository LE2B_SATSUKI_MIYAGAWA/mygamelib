#include "DirectXGame.h"
#include<DirectXMath.h>
#include<windows.h>
#include<DirectXMath.h>
#include<d3dcompiler.h>
#include<string>
#include<DirectXTex.h>
#include<wrl.h>
#include "3d/Object3d.h"
#include "Collision/SphereCollider.h"
#include "Collision/MeshCollider.h"
#include "Collision/CollisionManager.h"
#include "Player.h"
#include "3d/TouchableObject.h"

using namespace DirectX;
using namespace Microsoft::WRL;
using namespace std;

void DirectXGame::Initialize()
{
	//基底クラスの初期化
	GameBase::Initialize();

#pragma region シーン初期化
	// 最初のシーン
	scene = new TitleScene();
	//scene = new GameScene();
	//scene = new EndScene();
	
	sceneState = Scene::Title;

	// 最初のシーンの初期化
	scene->Initialize(dxBase.get(), input, audioManger.get(), textureManager.get(), Dcamera.get(), Bcamera.get(),
		gamepad.get(), debugText->GetInstance());
#pragma endregion
}

void DirectXGame::Update()
{
	//基底クラスの更新
	GameBase::Update();

	// シーン切り替え
	{
		// シーンクラスで指定した次のシーンを受け取る
		BaseScene* nextScene = scene->GetNextScene();
		sceneState = scene->GetScene();

		if (nextScene)	// nextSceneがnullでないとき
		{
			// 元のシーンを削除
			delete scene;

			// 次に指定したシーンを初期化
			nextScene->Initialize(dxBase.get(), input, audioManger.get(), textureManager.get(), Dcamera.get(),
				Bcamera.get(), gamepad.get(), debugText);

			// 現在のシーンに適用
			scene = nextScene;
		}
	}

	scene->Update();
}

void DirectXGame::Draw()
{
	if (sceneState == Scene::Game)
	{
		testfog->PreDrawScene(dxBase->GetCommandList());
		scene->Draw();
		scene->FogDraw();
		testfog->PostDrawScene(dxBase->GetCommandList());

		if (scene->GetSceneChengeFlag())
		{
			testfog->ChengeFadeOutFlag();
		}
	}
	else if (sceneState == Scene::End)
	{
		postEffect->PreDrawScene(dxBase->GetCommandList());
		scene->FogDraw();
		scene->Draw();
		scene->UIDraw();
		postEffect->PostDrawScene(dxBase->GetCommandList());

		if (scene->GetSceneChengeFlag())
		{
			postEffect->ChengeFadeOutFlag();
		}
	}
	else if (sceneState == Scene::Title)
	{
		testfog->PreDrawScene(dxBase->GetCommandList());
		scene->Draw();
		scene->FogDraw();
		testfog->PostDrawScene(dxBase->GetCommandList());
	}
	
	//描画開始
	dxBase->BeginDraw();
	//背面スプライト描画
	//ポストエフェクト;
	scene->BgDraw();
	if (sceneState == Scene::Game)
	{
		testfog->Draw(dxBase->GetCommandList());
	}
	else if (sceneState == Scene::End)
	{
		postEffect->Draw(dxBase->GetCommandList());
	}

	scene->UIDraw();
	dxBase->EndDraw();
	//ここまで描画コマンド
	flamerate->Wait();
}

void DirectXGame::Finalize()
{
	//基底クラスの後始末
	GameBase::Finalize();
}

